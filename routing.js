if (Meteor.isClient) {
  // counter starts at 0
  Session.setDefault('counter', 0);

  Template.hello.helpers({
    counter: function () {
      return Session.get('counter');
    }
  });

  Template.hello.events({
    'click button': function () {
      // increment the counter when button is clicked
      Session.set('counter', Session.get('counter') + 1);
    }
  });

  Template.addList.events({
    'submit form': function(event) {
      event.preventDefault();
      var listName = $("[name=listName]").val();
      if (listName) {
        Lists.insert({
          name: listName
        }, function(error, results) {
          Router.go('listPage', {_id: results});
        });
      }
      $("[name=listName]").val('');
    }
  });

  Template.lists.helpers({
    list: function() {
      return Lists.find({}, {sort: {name: 1}});
    }
  });

  Template.todos.helpers({
      'todo': function(){
          var currentListId = this._id;
          return Todos.find({listId: currentListId}, {sort: {createdAt: -1}});
      }
  });

  Template.addTodo.events({
    'submit form': function(event){
        event.preventDefault();
        var todoName = $('[name="todoName"]').val();
        var currentListId = this._id;
        Todos.insert({
            name: todoName,
            completed: false,
            createdAt: new Date(),
            listId: currentListId
        });
        $('[name="todoName"]').val('');
    }
  });

  Template.todoItem.events({
    'click .delete-todo': function(event){
        event.preventDefault();
        var confirm = window.confirm("Delete this task?");
        if (confirm) {
          var documentId = this._id;
          Todos.remove({ _id: documentId });
        }
    }
  });
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}

Router.configure({
  layoutTemplate: 'main'
})

Router.route('/', {
  // options for the route
  template: 'home',
  name: 'home'
});


Router.route('/register', {
  name: 'register'
});

Router.route('/login', {
  name: 'login'
});

Router.route('/list/:_id', function () {
  this.render('listPage', {
      data: function() {
        return Lists.findOne({_id: this.params._id});
      }
    })
}, {
  name: 'listPage'
});